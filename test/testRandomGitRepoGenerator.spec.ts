import { expect } from "chai";
import { uniq } from "lodash";
import {
    RandomActsOfGit,
    GitHubClient,
} from "../src/classes";
import TestGitClient from "./TestGitClient";

describe("Test Random Git Repo Generator", function() {
    this.timeout(200000);

    it("should generate a random issue URL from GitHub", async (): Promise<void> => {
        // Initialize a new mock git client
        const mockClient: GitHubClient = new TestGitClient;
        
        // Initialize a new generator
        const randomActsOfGit: RandomActsOfGit = new RandomActsOfGit(mockClient);

        // Declare the number of times to test for randomness
        const testBatch: number = 25;

        // Declare an empty result set to store the random repo strings
        const repoResults: Array<Promise<string>> = [];

        // Loop over the declared batch amount to ensure no two results are the same
        for (let i = 0; i < testBatch; i++) {
            const repoURL: Promise<string> = randomActsOfGit.getRandomIssueUrl();
            repoResults.push(repoURL);
        }

        // Resolve all of the promises
        const resolvedRepoResults: string[] = await Promise.all(repoResults);

        // The results should match the test batch
        expect(resolvedRepoResults.length).to.equal(testBatch);

        // Gather unique results from the repo list
        const uniqueRepoResults: string[] = uniq(resolvedRepoResults);

        // The unique results should match the test batch
        expect(uniqueRepoResults.length).to.equal(testBatch);
    });
});
