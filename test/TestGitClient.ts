import {
    AnyResponse,
    SearchIssuesParams
} from "@octokit/rest";
import { GitHubClient } from "../src/classes";

export default class TestGitClient implements GitHubClient {
    public async searchIssues(params: SearchIssuesParams): Promise<AnyResponse> {
        let itemsWithUrls: any[] = [];
        const total: number = 1010;

        for (let i = 0; i < total; i++) {
            itemsWithUrls[i] = { "html_url": `https://github.com/testorg/testrepo/issues/${i}` };
        }

        if (params.page) {
            itemsWithUrls = [ itemsWithUrls[params.page - 1] ];
        }

        return {
            "data": {
                "total_count": total,
                "incomplete_results": false,
                "items": itemsWithUrls,
            },
        } as AnyResponse;
    }
}
