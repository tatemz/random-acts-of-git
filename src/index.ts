#!/usr/bin/env node

import {
    GitHubClient,
    RandomActsOfGit,
} from "./classes";

const client: GitHubClient = new GitHubClient();

const rag: RandomActsOfGit = new RandomActsOfGit(client);
rag.getRandomIssueUrl()
    .then((repo: string): void => {
        console.log(repo); // tslint:disable-line no-console
    });
