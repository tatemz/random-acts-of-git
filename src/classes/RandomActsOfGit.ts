import {
    AnyResponse,
    SearchIssuesParams,
} from "@octokit/rest";
import {
    GitHubClient,
    Random,
} from ".";

export default class RandomActsOfGit {
    private _client: GitHubClient;

    public constructor(client: GitHubClient) {
        this._client = client;
    }

    public async getRandomIssueUrl(): Promise<string> {
        // Define pagination parameters for initial "all issues" search
        const paginationParams: SearchIssuesParams = {
            order: "desc",
            page: 1,
            per_page: 1,
            q: "* in:body is:public archived:false size:>0 no:assignee state:open is:issue comments:>0",
            sort: "updated",
        };

        // Get total issue list in order to fetch the total number of issues that match the criteria
        const totalissueList: AnyResponse = await this._client.searchIssues(paginationParams);

        // If possible, set the total issue, otherwise set to 1000
        const totalIssueCount: number = (totalissueList.data.total_count > 1000) ?
            1000 : totalissueList.data.total_count;

        // Select a random number between 1 and the total issue result count
        const randomNumberFromTotal: number = Random.getNumber(1, totalIssueCount);

        // Select the issue object from random page of the same search
        const randomIssue: AnyResponse = await this._client.searchIssues({
            ...paginationParams,
            page: randomNumberFromTotal,
        });

        // Return the issue url
        return randomIssue.data.items[0].html_url;
    }
}
