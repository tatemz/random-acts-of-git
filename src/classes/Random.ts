export default class Random {
    public static getNumber(min: number, max: number): number {
        // Round up the min
        min = Math.ceil(min);

        // Round down the max
        max = Math.floor(max);

        // Select random number between min and max
        // The maximum is inclusive and the minimum is inclusive
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
}
