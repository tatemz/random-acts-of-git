import Octokit from "@octokit/rest";
import {
    AnyResponse,
    SearchIssuesParams,
} from "@octokit/rest";
import { GITHUB_TOKEN } from "../resources/constants";

export default class GitHubClient {
    public async searchIssues(params: SearchIssuesParams ): Promise<AnyResponse> {
        const octokit: Octokit = new Octokit();
        octokit.authenticate({
            token: GITHUB_TOKEN,
            type: "token",
        });

        return octokit.search.issues(params);
    }
}
